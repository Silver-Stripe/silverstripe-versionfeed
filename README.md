# Version Feed

[![CI](https://github.com/silverstripe/silverstripe-versionfeed/actions/workflows/ci.yml/badge.svg)](https://github.com/silverstripe/silverstripe-versionfeed/actions/workflows/ci.yml)
[![Silverstripe supported module](https://img.shields.io/badge/silverstripe-supported-0071C4.svg)](https://www.silverstripe.org/software/addons/silverstripe-commercially-supported-module-list/)

## Overview

The module creates an RSS feed on each page with their change history, as well as one for the entire site.

## Requirements

 * Silverstripe ^4

**Note:** For a Silverstripe 3.x compatible version, please use [the 1.x release line](https://github.com/silverstripe/silverstripe-versionfeed/tree/1.2).

## Installation

Install with composer by running `composer require silverstripe/versionfeed` in the root of your Silverstripe project.

## Usage

For usage instructions see [user manual](docs/en/userguide/index.md).

## Translations

Translations of the natural language strings are managed through a third party translation interface, transifex.com. Newly added strings will be periodically uploaded there for translation, and any new translations will be merged back to the project source code.

Please use [https://www.transifex.com/projects/p/silverstripe-versionfeed](https://www.transifex.com/projects/p/silverstripe-versionfeed) to contribute translations, rather than sending pull requests with YAML files.
